extends SugarGame

"""
Base game class, takes care of loading and saving from file, and holds the state
"""

const DAY_SPLITS = 7 # How many splits does a day have

signal day_ended

signal time_of_day_changed

func _ready():
	#._ready()
	pass
	
func end_day():
	emit_signal("day_ended")
	emit_signal("time_of_day_changed")
	state.time_of_day = 0
	state.days_passed += 1
func advance_day():
	state.time_of_day += 1
	
	if state.time_of_day >= DAY_SPLITS:
		end_day()
	else:
		emit_signal("time_of_day_changed")