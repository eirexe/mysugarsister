extends SugarMinigame

onready var metronome = get_node("Metronome")
onready var pleasure_meter = get_node("PleasureMeter")

const PlayerAction = preload("res://game/minigames/h_minigame/player_action.gd")
const NinaCharacter = preload("res://game/minigames/h_minigame/nina_character.gd")
var nina : NinaCharacter = NinaCharacter.new()
enum BODY_PART {
		MOUTH,
		LEFT_HAND,
		RIGHT_HAND
	}
	
enum SPEED {
		LOW,
		MEDIUM,
		HIGH
	}


var speed = SPEED.MEDIUM

var player_actions = []

var actions_being_done = {
		BODY_PART.MOUTH: null,
		BODY_PART.LEFT_HAND: null,
		BODY_PART.RIGHT_HAND: null
	}
	
func add_action(action: PlayerAction, action_button):
	player_actions.append(action)
	action_button.action = action
	action_button.connect("action_selected", self, "_on_action_selected")
	
func _on_action_selected(action: PlayerAction):
	actions_being_done[action.body_part] = action
	_next_turn()
func _next_turn():
	for body_part in actions_being_done:
		var action = actions_being_done[body_part]
		if action:
			action.do_action(nina, speed)
	nina.on_turn()
	pleasure_meter.value = nina.pleasure
	print("NEXT TURN!")
	metronome.rythm = nina.get_player_touching_speed()
	_show_turn_text()
	
func create_player_actions():
	add_action(PlayerAction.new("H_GAME_KISS", BODY_PART.MOUTH), get_node("ActionButtons/KissButton"))
	
func _on_turn_text_finished():
	metronome.rythm_multiplier = 1.0
	$ActionButtons.show()
func _show_turn_text():
	$ActionButtons.hide()
	game.vn.connect("scene_finished", self, "_on_turn_text_finished", [], CONNECT_ONESHOT)
	# TODO: Make turn text work
	_on_turn_text_finished()
	
func _ready():
	game.vn.tie.hide()
	game.show()
#	game.vn.tie.set_background_opacity(0.5)
	create_player_actions()
	metronome.rythm = 0.0
	
func _on_exit_minigame():
	game.vn.tie.reset_background_opacity()