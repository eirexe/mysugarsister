extends Button
	
var action setget set_action

func set_action(_action):
	action = _action
	hint_tooltip = action.action_name

signal action_selected(action)

func _ready():
	connect("pressed", self, "_on_pressed")
	
func _on_pressed():
	if action:
		emit_signal("action_selected", action)