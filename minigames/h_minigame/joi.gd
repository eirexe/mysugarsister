extends Control

const NOTE_TEXTURE = preload("res://game/minigames/h_minigame/heart.svg")
const WAVE_PREVIEW_WIDTH = 400.0
const WAVE_PREVIEW_HEIGHT = 140.0
const WAVE_PREVIEW_OFFSET_TOP = 70.0
const FADE_IN_END_TIME = 0.25

var current_notes = []

var note_speed = 0.5 # the speed at which notes move, if 0.5 it should take 2 seconds from the side of the screen
var rythm = 60.0 # notes per minute
var _new_note_counter = 0 # When this is 1 or bigger a new note should be created
var _time = 0.0

var rythm_multiplier = 1.0

func _ready():
	var hslider = get_node("../DebugSlider")
	if not OS.is_debug_build():
		hslider.hide()
	else:
		hslider.value = rythm
		hslider.min_value = 10
		hslider.max_value = 230
		hslider.connect("value_changed", self, "_on_rythm_changed")

func t_wave(t, ampl, ord_freq, phase = 0.0):
	return ampl * sin((2*PI*ord_freq)*t + phase)

func _process(delta):
	_time += delta
	var wave = t_wave(_time, rythm * rythm_multiplier, note_speed)
	_new_note_counter += abs(wave / 60.0) * delta

	if _new_note_counter >= 1:
		current_notes.append(_new_note_counter - 1.0) # We add the leftover time to ensure better sync
		_new_note_counter = 0
		
	for i in range(current_notes.size() - 1, -1, -1):
		if current_notes[i] >= 1:
			# Plays sound and shows the effect
			current_notes.remove(i)
			$AudioStreamPlayer.play()
			$TextureRect/Particles2D.emitting = true
			$TextureRect/Particles2D.restart()
		else:
			current_notes[i] += delta * note_speed
	update()
	
	if OS.is_debug_build():
		update_debug()

func _on_rythm_changed(value: int):
	rythm = value

func update_debug():
	get_node("../Debug").text = "Creation rate: %.2f per minute\nNotes: %d\nCreation delta:%.2f\nRythm: %.2f" % [t_wave(_time, rythm, note_speed), current_notes.size(), _new_note_counter, rythm]


func _draw():
	# Draws the notes
	for note in current_notes:
		var texture_rect_size = $TextureRect.rect_size
		var location = rect_size.x-(rect_size.x/2*note) - texture_rect_size.x/2
		var opacity = clamp(note/FADE_IN_END_TIME, 0.0, 1.0)
		draw_texture_rect(NOTE_TEXTURE, Rect2(Vector2(location, rect_size.y/2 - texture_rect_size.x / 2), texture_rect_size), false, Color(1,1,1,opacity))
	"""
	if OS.is_debug_build():
		var wave_preview_points = []
		
		for i in range(WAVE_PREVIEW_WIDTH):
			var preview_time_diff =  (i - (WAVE_PREVIEW_WIDTH / 2.0)) / (WAVE_PREVIEW_WIDTH / 2.0) # how much to subtract from t to get the correct positions of the wave
			var preview_wave = t_wave(_time+preview_time_diff, rythm / 4.0, note_speed)
			wave_preview_points.append(Vector2(i, (-preview_wave) + WAVE_PREVIEW_HEIGHT))
		draw_line(Vector2(WAVE_PREVIEW_WIDTH / 2, WAVE_PREVIEW_OFFSET_TOP), Vector2(WAVE_PREVIEW_WIDTH / 2, WAVE_PREVIEW_HEIGHT+WAVE_PREVIEW_OFFSET_TOP), Color(1,1,1))
		draw_polyline(PoolVector2Array(wave_preview_points), Color(1,1,1))
	"""