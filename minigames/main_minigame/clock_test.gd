# Todo: Replace this by a good looking clock...

extends Control

const RECT_WIDTH = 50
const RECT_HEIGHT = 25

func _ready():
	GameManager.game.connect("time_of_day_changed", self, "update")

func _draw():
	for i in range(GameManager.game.DAY_SPLITS):
		var rect_pos = Vector2(RECT_WIDTH*i, 0)
		var rect_s = Vector2(RECT_WIDTH, RECT_HEIGHT)
		var rect_color := Color(1, 0, 0) if i == GameManager.game.state.time_of_day else Color(1, 1, 1)
		draw_rect(Rect2(rect_pos, rect_s), rect_color)