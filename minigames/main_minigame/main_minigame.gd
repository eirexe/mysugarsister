extends SugarMinigame

const LOW_AFFECTION = 0
const MEDIUM_AFFECTION = 150
const HIGH_AFFECTION = 300

var talk_lines = [
	{
		"path": "res://game/minigames/main_minigame/scenes/talk_low_affection.json",
		"threshold": LOW_AFFECTION
	},
	{
		"path": "res://game/minigames/main_minigame/scenes/talk_medium_affection.json",
		"threshold": MEDIUM_AFFECTION
	},
	{
		"path": "res://game/minigames/main_minigame/scenes/talk_high_affection.json",
		"threshold": HIGH_AFFECTION
	}
]

var bed_lines = [
	{
		"path": "res://game/minigames/main_minigame/scenes/bed_low_affection.json",
		"threshold": LOW_AFFECTION
	},
	{
		"path": "res://game/minigames/main_minigame/scenes/bed_middle_affection.json",
		"threshold": MEDIUM_AFFECTION
	},
	{
		"path": "res://game/minigames/main_minigame/scenes/bed_high_affection.json",
		"threshold": HIGH_AFFECTION
	}
]

var morning_lines = [
	{
		"path": "res://game/minigames/main_minigame/scenes/morning_low_affection.json",
		"threshold": LOW_AFFECTION
	},
	{
		"path": "res://game/minigames/main_minigame/scenes/morning_middle_affection.json",
		"threshold": MEDIUM_AFFECTION
	},
	{
		"path": "res://game/minigames/main_minigame/scenes/morning_high_affection.json",
		"threshold": HIGH_AFFECTION
	}
]

onready var interaction_panel = get_node("InteractionPanel")

func _ready():
	game.connect("day_ended", self, "on_day_ended")
	game.vn.tie.hide()

# Called it's time to end the day
func on_day_ended():
	interaction_panel.hide()
	game.vn.connect("scene_finished", self, "end_day", [], CONNECT_ONESHOT)
	run_random_subscenes_by_threshold(bed_lines, game.state.nina_state.affection)

# Ends the day with a fade out
func end_day():
	game.vn.tie.hide()
	$Fade/AnimationPlayer.play("FadeNight")
	$Fade.show()
	$Fade/AnimationPlayer.connect("animation_finished", self, "_on_new_day_fade_out_finished", [], CONNECT_ONESHOT)
	
func _on_new_day_fade_out_finished(animation):
	# Morning subscene
	$Fade.hide()
	interaction_panel.hide()
	game.vn.connect("scene_finished", self, "_on_day_start", [], CONNECT_ONESHOT)
	run_random_subscenes_by_threshold(morning_lines, game.state.nina_state.affection)

func _on_day_start():
	interaction_panel.show()
	game.vn.tie.hide()

func _on_talk():
	game.vn.connect("scene_finished", self, "_on_finished_talking", [], CONNECT_ONESHOT)
	run_random_subscenes_by_threshold(talk_lines, game.state.nina_state.affection)
	interaction_panel.hide()
	
func _on_finished_talking():
	game.vn.tie.hide()
	interaction_panel.show()
	game.advance_day()
func run_random_subscenes_by_threshold(scenes: Array, threshold: int):
	game.vn.tie.show()
	print("RUN!!!!")
	var selected_scene
	for scene in scenes:
		if threshold >= scene.threshold:
			selected_scene = scene
	play_random_subscene_from_file(selected_scene.path)

func _on_save_button_pressed():
	GameManager.game.pause_menu.show_save_game_prompt()